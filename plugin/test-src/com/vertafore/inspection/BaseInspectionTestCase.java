package com.vertafore.inspection;

import com.intellij.testFramework.InspectionTestCase;

/**
 * Created by IntelliJ IDEA.
 * User: bhandy
 * Date: 4/10/12
 * Time: 12:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaseInspectionTestCase extends InspectionTestCase {

    @Override
    protected String getTestDataPath() {
        return "./plugin/test-data";
    }

}
