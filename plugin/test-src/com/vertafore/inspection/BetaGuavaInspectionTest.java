package com.vertafore.inspection;

/**
 * @author bhandy
 */
public class BetaGuavaInspectionTest extends BaseInspectionTestCase {

    public void setUp() throws Exception {
        System.setProperty("idea.load.plugins.id", "com.intellij.modules.ultimate");
        super.setUp();
    }

    public void testBetaMethodCall() throws Exception {
        doTest();
    }

    public void testNonBetaMethodCall() throws Exception {
        doTest();
    }

    public void testFieldDeclarationOfClassTypeAnnotatedWithBeta() throws Exception {
        doTest();
    }

    public void testFieldDeclarationOfClassTypeNotAnnotatedWithBeta() throws Exception {
        doTest();
    }

    public void testLocalVariableDeclarationOfClassTypeAnnotatedWithBeta() throws Exception {
        doTest();
    }

    public void testLocalVariableDeclarationOfClassTypeNotAnnotatedWithBeta() throws Exception {
        doTest();
    }

    public void testBetaConstructorCall() throws Exception {
        doTest();
    }

    public void testNonBetaConstructorCall() throws Exception {
        doTest();
    }

    private void doTest() throws Exception {
        BetaGuavaInspection inspection = new BetaGuavaInspection();
        doTest("BetaGuava/" + getTestName(true), inspection);
    }

}
