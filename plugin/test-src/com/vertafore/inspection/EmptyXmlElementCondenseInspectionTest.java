package com.vertafore.inspection;

import com.intellij.codeInspection.LocalInspectionTool;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: bhandy
 * Date: 4/10/12
 * Time: 12:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class EmptyXmlElementCondenseInspectionTest extends BaseInspectionTestCase {
    
    private LocalInspectionTool _inspection;
    
    protected void setUp() throws Exception {
        _inspection = new EmptyXmlElementCondenseInspection();
        super.setUp();
    }

    protected void tearDown() throws Exception {
        _inspection = null;
        super.tearDown();
    }

    public void testEmptyElementDetection() throws Exception {
        doTest(EmptyXmlElementCondenseInspection.SHORT_NAME + "/emptyElementDetection", _inspection);
    }
    
    @Test
    public void verifyNoEmptyElementDetection() throws Exception {
        doTest(EmptyXmlElementCondenseInspection.SHORT_NAME + "/noEmptyElementDetection", _inspection);
    }

}
