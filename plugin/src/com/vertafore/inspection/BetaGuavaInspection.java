package com.vertafore.inspection;

import com.intellij.codeInsight.AnnotationUtil;
import com.intellij.codeInspection.BaseJavaLocalInspectionTool;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiClassType;
import com.intellij.psi.PsiDeclarationStatement;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiJavaCodeReferenceElement;
import com.intellij.psi.PsiLocalVariable;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiNewExpression;
import com.intellij.psi.PsiReferenceExpression;
import com.intellij.psi.PsiType;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Set;

/**
 * Inspection used to detect the usage of Guava methods annotated with the @Beta annotation.
 *
 * @author bhandy
 */
public class BetaGuavaInspection extends BaseJavaLocalInspectionTool {

    private static final String SHORT_NAME = "BetaGuava";
    private static final String DISPLAY_NAME = "Guava @Beta Artifact Usage";

    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    @Nls
    @NotNull
    @Override
    public String getGroupDisplayName() {
        return LansingJugInspectionProvider.GROUP_NAME;
    }

    @NotNull
    @Override
    public String getShortName() {
        return SHORT_NAME;
    }

    @Override
    public boolean isEnabledByDefault() {
        return true;
    }

    @NotNull
    @Override
    public PsiElementVisitor buildVisitor(final @NotNull ProblemsHolder holder, final boolean isOnTheFly) {
        final Set<String> affectedAnnotations = Collections.singleton("com.google.common.annotations.Beta");
        return new JavaElementVisitor() {

            /*
             * Processes calls to methods.  This would be instances like String.valueOf(new Long(5)).  The method
             * valueOf on String is the method call.
             */
            @Override
            public void visitMethodCallExpression(PsiMethodCallExpression expression) {
                PsiElement resolvedElement = expression.getMethodExpression().resolve();

                if (resolvedElement instanceof PsiMethod) {
                    PsiMethod method = (PsiMethod) resolvedElement;
                    if (AnnotationUtil.findAnnotationInHierarchy(method, affectedAnnotations) != null) {
                        registerProblem(holder, expression);
                    }
                }
            }

            /*
             * Processes field declarations within a class to determine if the class identifying the type of the field
             * is annotated with @Beta.
             */
            @Override
            public void visitField(PsiField field) {
                PsiType type = field.getType();
                if (type instanceof PsiClassType) {
                    PsiClass psiClass = ((PsiClassType) type).resolve();
                    if (AnnotationUtil.findAnnotationInHierarchy(psiClass, affectedAnnotations) != null) {
                        registerProblem(holder, field.getTypeElement());
                    }
                }
            }

            /*
             * Processes the parameters for method declaration to determine if the class type for any of the parameters
             * is annotated with @Beta.
             */
            @Override
            public void visitDeclarationStatement(PsiDeclarationStatement statement) {
                for (PsiElement element : statement.getDeclaredElements()) {
                    if (element instanceof PsiLocalVariable) {
                        PsiLocalVariable variable = (PsiLocalVariable) element;
                        PsiType type = variable.getType();
                        if (type instanceof PsiClassType) {
                            PsiClass psiClass = ((PsiClassType) type).resolve();
                            if (AnnotationUtil.findAnnotationInHierarchy(psiClass, affectedAnnotations) != null) {
                                registerProblem(holder, variable.getTypeElement());
                            }
                        }
                    }
                }
            }

            /*
             * Processes a constructor call to determine if the class being instantiated is annotated with @Beta.
             */
            @Override
            public void visitNewExpression(PsiNewExpression expression) {
                PsiJavaCodeReferenceElement reference = expression.getClassOrAnonymousClassReference();
                PsiElement element = (reference == null) ? null : reference.resolve();
                if (element instanceof PsiClass) {
                    PsiClass psiClass = (PsiClass) element;
                    if (AnnotationUtil.findAnnotationInHierarchy(psiClass, affectedAnnotations) != null) {
                        registerProblem(holder, reference);
                    }
                }
            }

            @Override
            public void visitReferenceExpression(PsiReferenceExpression expression) {
                // this is only needed to satisfy the abstract nature of JavaElementVisitor.
            }

            private void registerProblem(ProblemsHolder holder, PsiElement expression) {
                holder.registerProblem(expression,
                        "Unrecommended usage of @Beta annotated construct in Guava framework.");
            }

        };
    }

}
