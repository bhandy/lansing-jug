package com.vertafore.inspection.quickfix;

import com.intellij.codeInsight.CodeInsightUtil;
import com.intellij.codeInsight.CodeInsightUtilBase;
import com.intellij.codeInspection.LocalQuickFix;
import com.intellij.codeInspection.ProblemDescriptor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.ReadonlyStatusHandler;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;

/**
 * Quick Fix implementation to condense an empty tag into the short form.
 */
public class CondenseXmlElementQuickFix implements LocalQuickFix {
    
    @NotNull
    @Override
    public String getName() {
        return "Condense empty XML tag";
    }

    @NotNull
    @Override
    public String getFamilyName() {
        return "LansingJUG";
    }

    @Override
    public void applyFix(@NotNull Project project, @NotNull ProblemDescriptor problemDescriptor) {
        PsiElement problemElement = problemDescriptor.getPsiElement();
        if(!CodeInsightUtilBase.prepareFileForWrite(problemElement.getContainingFile())) {
            return;
        }

        PsiDocumentManager.getInstance(project).commitAllDocuments();

        if (! (problemElement instanceof XmlTag)) {
            throw new IncorrectOperationException("Only XML tags may be collapsed.");
        }

        XmlTag tag = (XmlTag) problemElement;
        tag.collapseIfEmpty();
    }
    
}
