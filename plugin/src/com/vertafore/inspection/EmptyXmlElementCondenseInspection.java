package com.vertafore.inspection;

import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.codeInspection.XmlSuppressableInspectionTool;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.XmlElementVisitor;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlTokenType;
import com.vertafore.inspection.quickfix.CondenseXmlElementQuickFix;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

/**
 * XML inspection to identify long form empty tags (&lt;tag>&lt;/tag>) and provide a quick fix to shorten the tag
 * (&lt;tag />).
 */
public class EmptyXmlElementCondenseInspection extends XmlSuppressableInspectionTool {
    
    static final String SHORT_NAME = "LongFormEmptyTag";
    static final String DISPLAY_NAME = "Long Form Empty Tag";

    @Nls
    @NotNull
    @Override
    public String getGroupDisplayName() {
        return LansingJugInspectionProvider.GROUP_NAME;
    }

    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return DISPLAY_NAME;
    }

    @NotNull
    @Override
    public String getShortName() {
        return SHORT_NAME;
    }

    @NotNull
    @Override
    public PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder, boolean isOnTheFly) {
        return new XmlElementVisitor() {
            @Override
            public void visitXmlTag(XmlTag tag) {
                if (tag.getSubTags().length == 0 && tag.getValue().getTrimmedText().length() == 0) {
                    PsiElement lastChild = tag.getLastChild();
                    if (lastChild != null) {
                        ASTNode lastChildNode = lastChild.getNode();
                        if (lastChildNode != null && lastChildNode.getElementType() != XmlTokenType.XML_EMPTY_ELEMENT_END) {
                            holder.registerProblem(tag, new TextRange(1, tag.getName().length() + 1),
                                    "The empty tag short form is more elegant.", new CondenseXmlElementQuickFix());
                        }
                    }
                }
            }
        };
    }
}
