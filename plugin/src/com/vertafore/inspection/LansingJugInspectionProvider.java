package com.vertafore.inspection;

import com.intellij.codeInspection.InspectionToolProvider;

/**
 * Created by IntelliJ IDEA.
 * User: bhandy
 * Date: 4/7/12
 * Time: 3:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class LansingJugInspectionProvider implements InspectionToolProvider {
    
    static final String GROUP_NAME = "Lansing JUG";

    @Override
    public Class[] getInspectionClasses() {
        return new Class[]{ EmptyXmlElementCondenseInspection.class };
    }

}
